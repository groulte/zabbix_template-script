#------------------------------------------------------------------------------------------------------------------------
#                                       	SCRIPT POWERSHELL WINDOWS UPDATE ZABBIX	            	                  	#
#Script permettant de r�cup�rer diff�rentes informations � partir de windows update et de les envoyer au serveur zabbix #
#                                                                                                                       #
#       D�pendances : zabbix																					        #
#                                                                                                                       #
#       R�alis� le : lundi 23 janvier 2017 � 09h00 par Arthur PAJOT - Stagiaire Code42 - BTS SIO SISR                   #
#       Editer le  : vendredi 17 fevrier 2017 � 09h00 par Arthur PAJOT - Stagiaire Code42 - BTS SIO SISR                #
#                                                                                                                       #
#------------------------------------------------------------------------------------------------------------------------
""
$politique = Get-ExecutionPolicy
if ($politique -eq "RemoteSigned") {echo "La politque d'execution powershell est bien configuree "} else {"ERROR - La politque d'execution powershell est mal configuree"}
""
$global:Zabbix_send = " & 'C:\Program Files\Zabbix Agent\zabbix_sender.exe' -c 'C:\ProgramData\zabbix\zabbix_agentd.conf'"
"Script debute:  " + (Get-Date -Format G)
"Computer Name: " + $env:COMPUTERNAME
""
Function Send_Zabbix {
# Sends Keys to Zabbix
#Expects [string]$key,int[value] with $global:Zabbix_send to be set to the valid path / args

    param (
        [parameter(mandatory=$true)]
        $key,
        
        [parameter(mandatory=$true)]
        $value
    )
    echo "$key : $value"
                $cmd = "$Zabbix_send -k `"$key`" -o `"$value`""
                echo "$cmd"
    Invoke-Expression $cmd 
    
}

echo "Calcul du nombre de mises a jour a installer..."
""

#Calcul du nombre de mises a jour
$UpdateSession = New-Object -ComObject Microsoft.Update.Session
$UpdateSearcher = $UpdateSession.CreateUpdateSearcher()
$SearchResult = $UpdateSearcher.Search("IsInstalled=0")
    [Object[]] $Critical = $SearchResult.updates | where { $_.MsrcSeverity -eq "Critical" }
    [Object[]] $Important = $SearchResult.updates | where { $_.MsrcSeverity -eq "Important" }
    [Object[]] $Moderate = $SearchResult.updates | where { $_.MsrcSeverity -eq "Moderate" }
    [Object[]] $Low = $SearchResult.updates | where { $_.MsrcSeverity -eq "Low" }
    [Object[]] $Unspecified = $SearchResult.updates | where { $_.MsrcSeverity -eq "Unspecified" }
    [Object[]] $Other = $SearchResult.updates | where { $_.MsrcSeverity -eq $null }
	$Importantes = if (($Critical.count)+($Important.count) -eq $null) {0} else {($Critical.count)+($Important.count)}
	$Facultatives = if (($Moderate.count)+($Low.count)+($Unspecified.count)+($Other.count) -eq $null) {0} else {($Moderate.count)+($Low.count)+($Unspecified.count)+($Other.count)}
	$Total = if (($SearchResult.updates.count) -eq $null) {0} else {($SearchResult.updates.count)}
	
#Envoie du nombre de mises a jour
  "MAJs Importantes : $Importantes"
  		$Key = "win_update[-nb_install_high]"
		$Value = $Importantes
		Send_Zabbix $Key $Value
		""
  "MAJs Facultatives : $Facultatives"
  		$Key = "win_update[-nb_install_optional]"
		$Value = $Facultatives
		Send_Zabbix $Key $Value
		""
  "MAJs Total : $Total"
  	    $Key = "win_update[-nb_install_total]"
		$Value = $Total
		Send_Zabbix $Key $Value
		""

  $windowsUpdateObject = New-Object -ComObject Microsoft.Update.AutoUpdate

#Envoie d'informations sur les mises a jour a installees a zabbix
#Pour obtenir les informations donner "oui" comme valeur a la variable "pending" ; donner "non" pour ne pas les envoyer
$pending = "oui"
if ($pending -eq "oui")
{
  echo "Chargement de la liste detailler des MAJs a installer a zabbix..."
  ""
  $index = 0
  
  if ($Total -eq 0)
  {
	$Key = "win_update[to_install_infos]"
	$Value = "Aucune mises a jour a installer"
	Send_Zabbix $Key $Value
  }
  else
  {
	foreach ($entry in $SearchResult.Updates)
	{
		$index++
		$num = "Update $index - "

		$titre = $entry.Title -replace ' \(KB[0-9]*\)'
		$kb = $entry.Title -replace '.*\((.*)\)','$1'

		$dl = " - Reboot:" + $entry.IsDownloaded
		$calculgravite = if ($entry.MsrcSeverity -eq $null) {"Autre"} else {$entry.MsrcSeverity}
		$gravite = " - Gravite:$calculgravite"
		$maj = $num + $kb + $titre + $dl + $gravite
		$Key = "win_update[to_install_infos]"
		$Value = $maj
		Send_Zabbix $Key $Value
		""
	}
  }
  echo "Envoie d'informations sur les mises a jour a installees : OK !"
}
else
{
  echo "Modifier la valeur de la variable pending pour charger la liste d�tailler des updates"
}
""
"Script termine:  " + (Get-Date -Format G)
""