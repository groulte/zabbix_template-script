# ------------------------------------------------------------------------------------------------------------------------
# SCRIPT POWERSHELL WINDOWS UPDATE ZABBIX
# Script permettant de r�cup�rer diff�rentes informations � partir de windows update et de les envoyer au serveur zabbix
# D�pendances : zabbix
# R�alis� le : lundi 23 janvier 2017 � 09h00 par Arthur PAJOT - Stagiaire Code42 - BTS SIO SISR
# Editer le  : vendredi 17 fevrier 2017 � 09h00 par Arthur PAJOT - Stagiaire Code42 - BTS SIO SISR
# ------------------------------------------------------------------------------------------------------------------------

$global:Zabbix_send = " & 'C:\Program Files\Zabbix Agent\zabbix_sender.exe' -c 'C:\ProgramData\zabbix\zabbix_agentd.conf'"

#echo $args.count;
$1 = "-script_version"
$2 = "-level_configuration"
$3 = "-day_install"
$4 = "-reboot"
$5 = "-last_search_date"
$6 = "-last_install_date"
$7 = "-last_search_day"
$8 = "-last_install_day"
$9 = "-uptime"
$10 = "-update_server"

if ($args.count -eq 0)
{
	exit 1;
}
else
{
	#Envoi de la version du script
	if ($args[0] -eq $1)
	{
		$preversion = New-Object System.Globalization.CultureInfo("en-US")
		[double]$version = 0.6
		return $version.ToString("f",$preversion)
	}
	#Envoi du niveau de configuration de windows update
	if ($args[0] -eq $2)
	{
		$objAutoUpdate = New-Object -ComObject "Microsoft.Update.AutoUpdate"
		$objSett = $objAutoUpdate.Settings
		$Value = $objSett.NotificationLevel
		return $Value
	}
	#Envoi de la date program�e d'installation des MAJs
	if ($args[0] -eq $3)
	{
		$objAutoUpdate = New-Object -ComObject "Microsoft.Update.AutoUpdate"
		$objSett = $objAutoUpdate.Settings
		$Value = $objSett.ScheduledInstallationDay
		return $Value
	}
	#Envoi d'un bool�en correspondant � la n�cessit� d'un red�marrage
	if ($args[0] -eq $4)
	{
		$objSysInfo = New-Object -ComObject "Microsoft.Update.SystemInfo"
		$Value = if ($objSysInfo.RebootRequired -eq $False) {0} else {1}
		return $Value
	}
	$windowsUpdateObject = New-Object -ComObject Microsoft.Update.AutoUpdate
	$dateActuel = [int](Get-Date).ToString("yyyyMMdd")
	#Envoi de la date de la derni�re recherche de MAJs
	if ($args[0] -eq $5)
	{
		if ($windowsUpdateObject.Results.lastSearchSuccessDate -eq $null)
		{
			$Value = "19990101"
			return $Value
		}
		else
		{
			$Value = $windowsUpdateObject.Results.lastSearchSuccessDate.ToString("yyyyMMdd")
			return $Value
		}
	}
	#Envoi de la date de la derni�re installation de MAJs
	if ($args[0] -eq $6)
	{
		if ($windowsUpdateObject.Results.lastInstallationSuccessDate -eq $null)
		{
			$Value = "19990101"
			return $Value
		}
		else
		{
			$Value = $windowsUpdateObject.Results.lastInstallationSuccessDate.ToString("yyyyMMdd")
			return $Value
		}
	}
	#Envoie de la difference de jours entre aujourd'hui et le jour de la derniere recherche de MAJs
	if ($args[0] -eq $7)
	{
		if ($windowsUpdateObject.Results.lastSearchSuccessDate -eq $null)
		{
			$dateSearch = 19990101
			$daySearch = ($dateActuel) - ($dateSearch)
			$Value = $daySearch
			return $Value
		}
		else
		{
			$dateSearch = [int]$windowsUpdateObject.Results.lastSearchSuccessDate.ToString("yyyyMMdd")
			$daySearch = ($dateActuel) - ($dateSearch)
			$Value = $daySearch
			return $Value
		}
	}
	#Envoie de la difference de jours entre aujourd'hui et le jour de la derniere installation de MAJs
	if ($args[0] -eq $8)
	{
		if ($windowsUpdateObject.Results.lastInstallationSuccessDate -eq $null)
		{
			$dateInstall = 19990101
			$dayInstall = ($dateActuel) - ($dateInstall)
			$Value = $dayInstall
			return $Value
		}
		else
		{
			$dateInstall = [int]$windowsUpdateObject.Results.lastInstallationSuccessDate.ToString("yyyyMMdd")
			$dayInstall = ($dateActuel) - ($dateInstall)
			$Value = $dayInstall
			return $Value
		}
	}
	#Envoi de l'uptime machine
	if ($args[0] -eq $9)
	{
		$computer = $Env:COMPUTERNAME
		$Computerobj = "" | select Uptime
		$wmi = Get-WmiObject -ComputerName $computer -Query "SELECT LastBootUpTime FROM Win32_OperatingSystem"
		$now = Get-Date
		$boottime = $wmi.ConvertToDateTime($wmi.LastBootUpTime)
		$uptime = $now - $boottime
		$d = $uptime.days * 86400
		$h = $uptime.hours * 3600
		$m = $uptime.Minutes * 60
		$s = $uptime.Seconds
		[long]$Value = ($d + $h + $m + $s) * 0.00028
		return $Value
	}
	if ($args[0] -eq $10)
	{
		$regKey = Get-ItemProperty -Path Registry::HKEY_LOCAL_MACHINE\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate -Name WUServer -ErrorAction SilentlyContinue
		if ($? -eq 0)
		{
			return "Default"
		}
		return $regKey.WUServer
	}
}