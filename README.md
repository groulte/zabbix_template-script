# README 
##### (utilisation)
S025-windows-update-zabbix.ps1

Réalisé le : vedredi 27 janvier 2017 à 09h00 par Arthur PAJOT - Stagiaire Code42 - BTS SIO SISR

Éditer le  : vendredi 17 fevrier 2017 à 09h00 par Arthur PAJOT - Stagiaire Code42 - BTS SIO SISR

### Synopsis :
"S025-windows-update-zabbix.ps1" et "S025-trapper-windows-update-zabbix.ps1" est un script powershell permettant de récupérer des informations issue du programme
Windows Updates et de les envoyés automatiquement à Zabbix.

"S025-windows-update-zabbix.ps1" récupère les inforations suivantes :

	* L’état du service Windows Update.
	* La version du script.
	* L'uptime de la machine
	* Si un redémarrage nécessaire.
	* Le niveau de configuration de Windows Update (4 choix possibles).
	* Le jour programmé d’installation des mises à jour.
	* Le nombre de jours depuis la dernière recherche de mises à jour.
	* Le nombre de jours depuis la dernière installation de mises à jour.
	* La date de la dernière recherche de mises à jour.
	* La date de la dernière installation des mises à jours.

"S025-trapper-windows-update-zabbix.ps1" récupère les infromations suivantes :

	* Le nombre de MAJs importantes disponibles.
	* Le nombre de MAJs facultatives disponibles.
	* Le nombre de MAJs total disponibles.
	* Les informations sur les MAJs  disponibles.

### Pré-requis :

    * Avoir lu et effectué les étapes du fichier "INSTALL.md".
	* Un serveur zabbix.
	* Des hôtes (Windows) disposant de l'agent zabbix.
	* Un accès à internet.
	* Les values mappings asscoiés à ce script configuré sur l'interface web de zabbix (cf. INSTALL.md).
	* Le template "M025 - Windows Update Local.xml" importé dans l'interface web de zabbix (cf. INSTALL.md).
    * Disposer de Microsoft Windows Powershell 2.0 minimum
	* Vérifier que la politique d'exécution de Powershell est configuré sur "RemoteSigned".

### Attention :

Si le degré de configuration de votre Windows Update n'est pas établie sur "installer automatiquement les MAJs",
une erreur appraraitra sur Zabbix pour l'item "Date de la dernière installation de MAJs".

### Utilisation  :

    * Lancez Powershell, et mettez vous à l'emmplacement où se situe le script. 

    * Pour remonter les informations de Windows Update : ./S025-trapper-windows-update-zabbix.ps1
	
Nota : les autres informations, sont envoyés automatiquement par le script "S025-windows-update-zabbix.ps1".


### Historique des version :

##### "S025-windows-update-zabbix.ps1" Version 0.6 :

	* Envoi des infromations de manière active.


##### "S025-windows-update-zabbix.ps1" Version 0.5 :

    * Corrections de divers problèmes rencontrés lors des périodes de tests : 
        * Amélioration des logs (plus de verbosité).
	    * Uptime machine (transofrmation en entier).
	    * Bug d'envoi s'il n'y a pas d'updade.
	    * Bugs lors de la tâche planifiée :
		    * Pas d'envoi de la valeur de nécessite d'un redémarrage.
		    * Pas d'envoi du script en tant que nombre décimal.
	    * Bug d'envoi d'information sur les mises à jour à installer s'il n'y en a pas > envoi d'un texte quand même.
	    * Bug des accents sur les informations des updates a installees.


    * Création de fichiers :
	    * README
	    * INSTALL

    * Renommage configuration :
	    * Template.
	    * Application.
	    * Items.
	    * Triggers.
	    * Clées des triggers.
	    * Graph.
	    * Value mapping.

    * Amélioration de l'organisation du dépôt BitBucket.

    * Création d'un graph conenant :
	    * Nb des mises à jour (total, importantes et facultatives
	    * Redémarrage
	    * Uptime de la machine
	    * Valeur du jour programmé d'installation

    * Ajouts de fonctionnalités :
	    * Récupération + envoi du numero du script en tant que nombre décimal.
	    * Récupération + envoi de l'uptime machine.
	    * Récupération + envoi des infromations sur les mises à jour à installer.
	    * Création automatique du fichier de logs.
	    * Génération de logs.
	    * Récupération + envoi de la version du script (version du script donnée en paramètre de ce dernier).
	    * Gestion du chemin d'accès du fichier de configuration de zabbix et de zabbix sender.
	    * Récupération du nombre de jours depuis la dernière recherche de mises à jour.  
	    * Récupération du nombre de jours depuis la date de la dernière installation des mises à jours.  
	    * Récupération + envoi du degré de configuration de Windows Update.
	    * Récupération + envoi de l’état du service Windows Update.
	    * Récupération + envoi du nombre de mises à jour total.
	    * Récupération + envoi du nombre de mises à jour importantes.
	    * Récupération + envoi du nombre de mises à jour facultatives.
	    * Récupération + envoi de la date de la dernière recherche de mises à jour.  
	    * Récupération + envoi de la date de la dernière installation des mises à jours.   
	    * Récupération + envoi du jour programmé d’installation des mises à jour.
	    * Récupération + envoi de la nécessité d'un redémarrage.
	    * Récuperation + envoi de l'adresse du serveur de mise a jour.
