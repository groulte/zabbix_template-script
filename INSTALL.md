# INSTALL
##### (installation)
S025-windows-update-zabbix.ps1

Réalisé le : vedredi 27 janvier 2017 à 09h00 par Arthur PAJOT - Stagiaire Code42 - BTS SIO SISR

Éditer le  : vendredi 17 fevrier 2017 à 09h00 par Arthur PAJOT - Stagiaire Code42 - BTS SIO SISR

### Synopsis :
"S025-windows-update-zabbix.ps1" est un script powershell permettant de récupérer des informations issue du programme
Windows Updates et de les envoyés à Zabbix.

Ce script récupère les informations suivantes :

	* L’état du service Windows Update.
	* La version du script.
	* L'uptime de la machine
	* Si un redémarrage nécessaire.
	* Le niveau de configuration de Windows Update (4 choix possibles).
	* Le jour programmé d’installation des mises à jour.
	* Le nombre de jours depuis la dernière recherche de mises à jour.
	* Le nombre de jours depuis la dernière installation de mises à jour.
	* La date de la dernière recherche de mises à jour.
	* La date de la dernière installation des mises à jours.

### Pré-requis :

	* Un serveur zabbix.
	* Des hôtes (Windows) disposant de l'agent zabbix.
	* Vérifier qu'après l'installation de l'agent zabbix, vous disposez d'un dossier "Zabbix Agent" contenant le dossier "dev" et les fichies "zabbix_agentd.conf" et "zabbix_sender.exe" à l'emplacement "C:\Program Files\Zabbix Agent".
	* Que le(s) hôte(s) soi(en)t supérvisé(s) dans zabbix.
	* Un accès à internet.

### Étapes à réaliser avant l'utilisation du script :

	* 1- Mise en place des ressources nécessaire via le dépôt BitBucket.
	* 2- Création des values mapping sur l'interface web de zabbix.
	* 3- Importation le template dans l'interface web de zabbix.
	* 4- Associé le template à un hôte.
	* 5- Modification du fichier de configuration de l'agent zabbix.
	* 6- RTFM.

### 1- Mise en place des ressources nécessaire via le dépôt BitBucket :

	* Télécharger l’utilitaire "Git SCM" sur votre poste et installez-le.
	* Lancer le programme "Git GUI".
	* Cliquer sur "Clone Existing Repository".
	* Dans le champ "Source Location" coller cette adresse :
      "https://code42_stagiaire@bitbucket.org/groulte/zabbix_template-script.git".
	* Dans le champ "Target Directory" coller ce chemin d’accès : "C:\Program Files\Zabbix Agent\dev\SCRIPTS".
	 Attention, le chemin d'accès doit obligatoirement comprendre le dossier "Zabbix Agent" puis il doit être suivi d'un dossier non existant.
	* Cliquer sur "Clone".
	* Renseigner le mot de passe du dépôt BitBucket.
	* Le script est maintenant disponible sur la machine.
	* Maintenant, dépalcez le script "S025-windows-update-zabbix.ps1" à l'empalcement suviatn : "C:\"

### 2- Création des values mapping sur l'interface web de zabbix :

	* Aller sur l’interface web de votre serveur Zabbix et cliquez sur "Administration".
	* En haut à droite cliquez sur "GUI" et sélectionnz "Value mapping".
	* Cliquez maintenant, en haut à droite, sur "Create value map".
	* Donner un nom dans le champ "name" à cette Value Mapping.
	* Il faut ensuite donner un nom à la vallue mapping puis lui indiquer une valueur associé à un nom
	* Il faut cliquer sur le petit "Add" à chaque ajout de valeur.$
	* Une fois terminé, cliquez sur le grand "Add".

    Il y a 3 values mappings à créer, ci dessous les "names" :
        * VM025 - Jour programmé d’installation des mises à jour
        * VM025 - Niveau de configuration
        * VM025 - Redémarrage nécessaire ?

    Voici, ci-dessous, les associations valeurs / nom :
    * "VM025 - Jour programmé d’installation des mises à jour" prend pour valeurs :
        (Value) --> (Mapped to)
        * 0 --> Tous les jours
        * 1 --> Tous les dimanches
        * 2 --> Tous les lundis
        * 3 --> Tous les mardis
        * 4 --> Tous les mercredis
        * 5 --> Tous les jeudis
        * 6 --> Tous les vendredis
        * 7 --> Tous les samedis
        
     * "VM025 - Niveau de configuration" prend pour valeurs :
        (Value) --> (Mapped to)
        * 1 --> Ne pas rechercher
        * 2 --> Rechercher
        * 3 --> Télécharger
        * 4 --> Installer
        
    * "VM025 - Redémarrage nécessaire ?" prend pour valeurs :
        (Value) --> (Mapped to)
        * 0 --> OUI
        * 1 --> NON

### 3- Importation le template dans l'interface web de zabbix :
	* Télécharger le fichier nommée "M025 - Windows Update Local.xml".
	* Aller sur l’interface web de votre serveur Zabbix et cliquez sur "Configuration", "Template", "Import".
	* Cliquez maintenant sur le bouton "Choisir un fichier" et aller chercher le template précédemment téléchargé : 
	  "M025 - Windows Update Local.xml" puis cliquez sur "Ouvrir".
	* Cochez les règles d’importations selon votre convenance puis cliquez sur "Import".
	* Le template est importé dans votre environnement.

### 4- Associé le template à un hôte.
	* Allez dans l’onglet "Configuration" > "Hosts".
	* Cliquez sur le nom de l'hôte auquel vous souhaiter ajouter le tempalte windows updates.
	* Puis cliquez sur “Templates” enfin sélectionner “M025 - Windows Updates Local” et cliquez sur “Add” puis sur “Update”.

### 5- Modification du fichier de configuration de l'agent zabbix :
	* Ouvrez le fichier nommé "zabbix_agentd.conf" qui se trouve à l'empalcement "C:\Program Files\Zabbix Agent".
	
	* Décommentez la variable "LogRemoteCommand" et donner lui commme valeur "1".
	* Décommentez la variable "StartAgents" et donner lui commme valeur "10".
	* Décommentez la variable "BufferSend" et donner lui commme valeur "10".
	* Décommentez la variable "BufferSize" et donner lui commme valeur "150".
	* Décommentez la variable "Timeout" et donner lui commme valeur "30".
	* Supprimer la ligne "UserParameter=" et colez celle-ci à la place "UserParameter=win_update[*],%SystemRoot%\system32\WindowsPowerShell\v1.0\powershell.exe -nologo -command "& C:\S025-windows-update-zabbix.ps1" "$1" "$2" "$3" "$4" "$5" "$6" "$7" "$8" "$9" "$10""
	

### 6- RTFM :
Liser le fichier "README.md" qui vous expliquera comment bien utiliser le script.